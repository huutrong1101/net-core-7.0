﻿using Ex1.model.Model;

namespace Ex1.business.Services.Interfaces;

public interface IPostService
{
    Task<List<PostModel>> GetAllPosts();
}