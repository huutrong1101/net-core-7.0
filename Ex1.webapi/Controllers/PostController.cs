﻿using Ex1.business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Ex1.webapi.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class PostController: ControllerBase
{
    private readonly IPostService _postService;

    public PostController(IPostService postService)
    {
        _postService = postService;
    }
    
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var posts = await _postService.GetAllPosts();
        
        if (posts.Any())
        {
            return Ok(posts);
        }

        return NotFound();
    }
}