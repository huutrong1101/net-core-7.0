﻿using System.Text.Json;
using Ex1.business.Api.Interfaces;
using Ex1.model.Model;

namespace Ex1.business.Api.Implements;

public class PostApi: IPostApi
{
    public async Task<List<PostModel>> GetAllPost()
    {
        HttpClient client = new HttpClient();
        var response = await client.GetAsync("https://jsonplaceholder.typicode.com/posts");
        var content = await response.Content.ReadAsStringAsync();
        var posts = JsonSerializer.Deserialize<List<PostModel>>(content);

        posts ??= new List<PostModel>();
        
        return posts;
    }
}