﻿using System.ComponentModel;
using System.Text.Json;
using Ex1.business.Api.Interfaces;
using Ex1.business.Services.Interfaces;
using Ex1.model.Model;

namespace Ex1.business.Services.Implements;

public class PostService: IPostService
{
     private readonly IPostApi _postApi;

     public PostService(IPostApi postApi)
     {
          _postApi = postApi;
     }

     public Task<List<PostModel>> GetAllPosts()
     {
          var lsTPost = _postApi.GetAllPost();

          return lsTPost;
     }
}