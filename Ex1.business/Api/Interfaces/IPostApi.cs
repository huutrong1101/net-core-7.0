﻿using Ex1.model.Model;

namespace Ex1.business.Api.Interfaces;

public interface IPostApi
{
    public Task<List<PostModel>> GetAllPost();
}