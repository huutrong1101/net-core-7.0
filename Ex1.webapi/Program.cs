using Ex1.business.Api.Implements;
using Ex1.business.Api.Interfaces;
using Ex1.business.Services.Implements;
using Ex1.business.Services.Interfaces;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IPostService, PostService>();
builder.Services.AddSingleton<IPostApi, PostApi>();


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();